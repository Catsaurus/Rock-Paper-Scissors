const games = {};
let gameId = 0;

export const NEW_GAME_ADDED = 'NEW_GAME_ADDED';

/*export const MOVES_SET = 'MOVES_SET';


export const WAITING_FOR_MOVE = 'waiting_for_move';
export const FINISHED = 'finished';
export const TIE = 'TIE';
export const WIN = 'WIN';
export const LOSS = 'LOSS';*/
// game id
// game status
// moves = []

export const addnewGame = ({gameId, status}) => ({
    type: NEW_GAME_ADDED,
    gameId: gameId++,
    status: status
  })


/*export const movesSet = (move) => (
    {
        type: MOVES_SET,
        game: gameId
    })
    */