import InputChangesOnSubmit from '../components/InputChangesOnSubmit';
import {connect} from 'react-redux';
import {addnewGame} from '../actions/action';

const mapDispatchToProps = (state) => ({
    onSubmit: ({gameId, status}) => dispatch(addNewGame({gameId, status}))
});
const mapStateToProps = undefined;
export default connect(mapStateToProps, mapDispatchToProps)(InputChangesOnSubmit);