import { combineReducers } from 'redux'
import hangman from './hangman'

export default combineReducers({
  hangman
})