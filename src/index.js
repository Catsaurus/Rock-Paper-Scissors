/* global document */
import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/App';

import {Provider} from 'react-redux';
import {createStore} from 'redux';


let store = createStore(
    // reducer,
     window.devToolsExtension ? window.devToolsExtension() : (f) => f
)


ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);
